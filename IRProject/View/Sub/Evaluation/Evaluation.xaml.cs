﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System.Windows.Media;

namespace IRProject.View.Sub.Evaluation
{
    /// <summary>
    /// Interaction logic for Evaluation.xaml
    /// </summary>
    public partial class Evaluation : UserControl
    {
        public Evaluation()
        {
            InitializeComponent();

            ViewModel.EvaluationVM.Evaluation = this;
            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Curve",
                    Values=ViewModel.EvaluationVM.Points,
                    LineSmoothness = 0,
                    PointForeground=SystemColors.ScrollBarBrush,
                }
            };

            //XFormatter = val => val/val;
            //YFormatter = val => val / val;

            ((View.Sub.Evaluation.Evaluation)ViewModel.EvaluationVM.Evaluation).livechart.DataContext = this;
        }
        public SeriesCollection SeriesCollection { get; set; }
        public Func<double> XFormatter { get; set; }
        public Func<double> YFormatter { get; set; }

    }
}
