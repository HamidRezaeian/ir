﻿using NHazm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IRProject.View.Sub.Result
{
    /// <summary>
    /// Interaction logic for Model.xaml
    /// </summary>
    public partial class Model : UserControl
    {
        Normalizer norm = new Normalizer();
        public Model()
        {
            InitializeComponent();
            this.Loaded += Model_Loaded;
            TextDocName.PreviewMouseLeftButtonUp += TextDocName_PreviewMouseLeftButtonUp;
        }

        private void Model_Loaded(object sender, RoutedEventArgs e)
        {
            try {
                int index = 0;
                foreach (var item in IRProject.Model.StaticData.QueriesData.Where(i1 => i1.Query == ((View.Sub.SearchBox.SearchBox)(ViewModel.SearchBoxVM.SearchBox)).textBoxSearch.Text).Select(i1 => i1.Index))
                {
                    index = item;
                }
                foreach (var item in IRProject.Model.StaticData.RelevantData.Where(i => i.Index == index).ToList()[0].RelevantDocs)
                {
                    if (item == TextDocName.Text.Split('.')[0])
                        BorderRelevant.Background = Brushes.Green;
                }
            }
            catch { };
        }

        private void highlightWord(string s)
        {
            Regex reg = new Regex(s,RegexOptions.RightToLeft);


            TextPointer position = richTextBox.Document.ContentStart;
            List<TextRange> ranges = new List<TextRange>();
            while (position != null)
            {
                if (position.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                {
                    string text = position.GetTextInRun(LogicalDirection.Forward);
                    var matchs = reg.Matches(text);
                    foreach (Match match in matchs)
                    {

                        TextPointer start = position.GetPositionAtOffset(match.Index);
                        TextPointer end = start.GetPositionAtOffset(s.Length);

                        TextRange textrange = new TextRange(start, end);
                        ranges.Add(textrange);
                    }
                }
                position = position.GetNextContextPosition(LogicalDirection.Forward);
            }


            foreach (TextRange range in ranges)
            {
                range.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.Red));
                range.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
            }

        }
        private void TextDocName_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (richTextBox.Height == 0)
            {
                richTextBox.Height = 300;
                if (richTextBox.Document.Blocks.Count() == 1)
                {
                    string myText = File.ReadAllText(IRProject.Model.StaticData.HamshahriCorpusPath + TextDocName.Text, Encoding.UTF8);
                    myText = norm.Run(myText);
                    richTextBox.AppendText(myText);
                    foreach (var word in IRProject.Model.StaticData.Query)
                    {
                        if (IRProject.Model.StaticData.StopWordData.Where(i=>i==word.term).Count()==0)
                        highlightWord(word.term);
                    }
                    
                }
            }
            else
            {
                richTextBox.Height = 0;
            }
        }
    }
}
