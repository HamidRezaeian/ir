﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
namespace IRProject.View.Sub.SearchBox
{
    /// <summary>
    /// Interaction logic for SearchBox.xaml
    /// </summary>
    public partial class SearchBox : UserControl
    {
        public SearchBox()
        {
            InitializeComponent();
            ViewModel.SearchBoxVM.SearchBox = this;
            comboBox.GotFocus += ComboBox_GotFocus;
            buttonSearch.PreviewMouseLeftButtonUp += ButtonSearch_PreviewMouseLeftButtonUp;
        }



        private void ComboBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (comboBox.Items.Count==0)
            foreach (var item in IRProject.Model.StaticData.QueriesData.Select(i => i.Query))
            {
                comboBox.Items.Add(item);
            }
        }

        Model.InformationRetrival.InformationRetrival IR = new Model.InformationRetrival.InformationRetrival();
        private void ButtonSearch_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            int i = 0;
            Stopwatch watch = new Stopwatch();
            watch.Start();

            ((View.Sub.Result.Result)ViewModel.ResultVM.Result).StackPanel.Children.Clear();
            foreach (var item in IR.Search(textBoxSearch.Text))
            {
                i++;
                View.Sub.Result.Model model = new Result.Model();
                model.TextDocName.Text = item.DocName;
                model.TextScore.Text =Math.Round(item.Score,3).ToString();
                model.richTextBox.Height = 0;
                ((View.Sub.Result.Result)ViewModel.ResultVM.Result).StackPanel.Children.Add(model);
                if (i >= Model.StaticData.MaxResult) break;
            }
            watch.Stop();
            ((View.Sub.Result.Result)ViewModel.ResultVM.Result).labelResult.Content =
                string.Format("{0} results ({1} Seconds)",Model.StaticData.InformationRetreival.Count(),Math.Round(watch.Elapsed.TotalSeconds,2));




            Model.Model.EvaluationModel evaluationmodel = new Model.Model.EvaluationModel();
            bool relevantQ = true;
            int index = 0;
            foreach (var item in IRProject.Model.StaticData.QueriesData.Where(i1 => i1.Query == textBoxSearch.Text).Select(i1 => i1.Index))
            {
                index = item;
            }
            IRProject.Model.Formula.Evaluation.Calculate
                (IRProject.Model.StaticData.InformationRetreival.Select(i1 => i1.DocName).ToList(),index,
                  ref evaluationmodel.Precision, ref evaluationmodel.Recall, ref evaluationmodel.MAP, ref evaluationmodel.FMeasure,ref relevantQ);
            evaluationmodel.query = textBoxSearch.Text;



            if (relevantQ)
            {
                ViewModel.EvaluationVM.SetData(evaluationmodel);
                if (IRProject.Model.StaticData.EvaluationData.Where(i1 => i1.query == evaluationmodel.query).Count() == 0)
                    IRProject.Model.StaticData.EvaluationData.Add(evaluationmodel);
            }
            else
            {
                ((View.Main.Main)(ViewModel.MainVM.Main)).Evaluation.Width = 0;
            }
        }

    }
}
