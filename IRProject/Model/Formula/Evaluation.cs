﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.Formula
{
    public static class Evaluation
    {
        
        public static void Calculate(List<string> Docs,int Queryindex,ref List<double> Precision,ref List<double> Recall,ref double MAP,ref double Fmeasure,ref bool relevantsqueries)
        {
            relevantsqueries = true;
            double precision = 0;
            List<double> AllPrecision = StaticData.EvaluationData.Select(i => i.MAP).ToList();
            int relevantcount = 0;
            int AllRelevants = 0;
            try
            {
                AllRelevants = StaticData.RelevantData.Where(i => i.Index == Queryindex).ToList()[0].RelevantDocs.Count();
            }
            catch { relevantsqueries = false; }
            for (int i = 1; i <= StaticData.MaxResult; i++)
            {
                if (StaticData.RelevantData.Where(i1 => i1.Index == Queryindex && i1.RelevantDocs.Where(i2 => i2 == Docs[i - 1].Split('.')[0]).Count() > 0).Count()>0)
                    {
                        relevantcount++;
                        Precision.Add((double)relevantcount / (double)i);
                        Recall.Add((double)relevantcount / (double)AllRelevants);
                        precision += (double)relevantcount / (double)i;
                    }
                    else
                    {
                        if (relevantcount > 0)
                        {
                            Precision.Add((double)relevantcount / (double)i);
                            Recall.Add((double)relevantcount / (double)AllRelevants);
                        }
                        else
                        {
                            Precision.Add(0);
                            Recall.Add(0);
                        }
                    }
            }
            precision = (double)precision / (double)relevantcount;
            if (double.IsNaN(precision))
                precision = 0;

            AllPrecision.Add(precision);
            foreach (var p in AllPrecision)
            {
                MAP += (double)p;
            }
            MAP = (double)MAP / (double)AllPrecision.Count();

            double P= (double)relevantcount / (double)StaticData.MaxResult;
            double R = ((double)relevantcount / (double)AllRelevants);
            Fmeasure =(double)(2*P*R)/(double)(P+ R);
            if (double.IsNaN(Fmeasure))
                Fmeasure = 0;

        }
    }
}
