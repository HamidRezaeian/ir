﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.Formula
{
    public static class TfIdf
    {
        public static double CalculateLN(double tf)
        {
            tf = 1 + Math.Log10(tf);
            return tf;
        }
        public static double CalculateLT(double tf, double df)
        {
            tf = 1 + Math.Log10(tf);
            df = Math.Log10(StaticData.N / df);
            return tf * df;
        }
        public static List<double> CalculateCosine(List<double> wt)
        {
            double Denominator = 0;
            List<double> cosine = new List<double>();
            foreach (var w in wt)
            {
                Denominator +=Math.Pow(w,2);
            }
            Denominator = Math.Sqrt(Denominator);
            foreach (var w in wt)
            {
                cosine.Add(w / Denominator);
            }
            return cosine;
        }
        public static double CalculateProd(List<double> w1, List<double> w2)
        {
            double prod = 0;
            //foreach (var qx in q)
            //{
            //    foreach (var dx in d)
            //    {
            //        if (qx.term == dx.term)
            //            prod += qx.WT * dx.WT;
            //    }
            //}
            for (int i = 0; i < w1.Count(); i++)
            {
                prod += w1[i] * w2[i];
            }
            return prod;
        }
    }
}
