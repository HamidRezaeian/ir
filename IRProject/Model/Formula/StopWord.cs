﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.Formula
{
    public static class StopWord
    {
        public static bool IsStopWord(int tf,string term)
        {
            double x =(double) tf / StaticData.TotalTerm;
            if (x > 0.045)
                return true;
            return false;
        }
    }
}
