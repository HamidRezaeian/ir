﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.CreateInformation
{
    public class CreateStopWord
    {
        ReadFile read = new ReadFile();
        public bool Create()
        {
            try
            {
                StaticData.TFData = read.ReadTFData();
                StaticData.StopWordData.Clear();
                foreach (var t in StaticData.TFData)
                {
                    Int32 totaltf=0;
                    foreach (var tf in StaticData.TFData.Where(i=>i.term==t.term))
                    {
                        totaltf += tf.tf;
                    }
                    if (Formula.StopWord.IsStopWord(totaltf,t.term))
                        StaticData.StopWordData.Add(t.term);
                }
                StaticData.StopWordData = StaticData.StopWordData.Distinct().ToList();
                //List<string> temp = Formula.StopWord.temp;
                Write2file(StaticData.StopWordData);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private static bool Write2file(List<string> Document)
        {
            try
            {
                File.WriteAllText(StaticData.StopWordPath + StaticData.StopWordFileName, Converter.ConvertStopWordData2String(Document));
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
