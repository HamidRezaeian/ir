﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.CreateInformation
{
    public class ReadFile
    {
        public List<Model.DocumentModel> ReadDocuments()
        {
            PreProcess.PreProcess preprocess = new PreProcess.PreProcess();
            List<Model.DocumentModel> documents = new List<Model.DocumentModel>();
            OpenFileDialog op1 = new OpenFileDialog();
            op1.Multiselect = true;
            op1.ShowDialog();
            
            string[] path = op1.FileNames;
            foreach (var doc in path)
            {
                Model.DocumentModel model = new Model.DocumentModel();
                model.DocumentText = File.ReadAllText(doc);
                model.DocumentText = model.DocumentText.Replace('\n', ' ');
                model.DocumentText = preprocess.RUN(model.DocumentText);
                string[] splitpath= doc.Split('\\');
                model.DocumentName = splitpath[splitpath.Count()-1];
                documents.Add(model);
            }
            return documents;
        }
        public List<Model.TFModel> ReadTFData()
        {
            StaticData.TotalTerm = 0;
            List<Model.TFModel> TFdata = new List<Model.TFModel>();
            string[] tfpath = Directory.GetFiles(StaticData.TFDataPath);
            foreach (var doc in tfpath)
            {
                string readdoc = File.ReadAllText(doc);
               // Model.TFModel tfmodel = new Model.TFModel();
                bool linetf = true;
                string term = "";
                foreach (var row in readdoc.Split('\n'))
                {
                    
                    if (linetf)
                    {
                        term = row;
                        //tfmodel.term = row;
                        linetf = false;
                    }
                    else
                    {
                        Model.TFModel tfmodel = new Model.TFModel();
                        tfmodel.term = term;
                        tfmodel.tf = Convert.ToInt32(row);
                        StaticData.TotalTerm += tfmodel.tf;
                        linetf = true;
                        string[] splitpath = doc.Split('\\');
                        tfmodel.DocName = splitpath[splitpath.Count() - 1];
                        TFdata.Add(tfmodel);
                    }

                }
            }
            return TFdata;
        }
        public List<Model.DFModel> ReadDFData()
        {
            List<Model.DFModel> DFdata = new List<Model.DFModel>();
            string[] dfpath = Directory.GetFiles(StaticData.DFDataPath);
            foreach (var doc in dfpath)
            {
                string readdoc = File.ReadAllText(doc);
                foreach (var row in readdoc.Split('\n'))
                {
                    try {
                        Model.DFModel dfmodel = new Model.DFModel();
                        List<string> onerow = row.Split(' ').ToList();
                        onerow.Remove(onerow[onerow.Count - 1]);
                        dfmodel.Term = onerow[0];
                        onerow.Remove(onerow[0]);
                        dfmodel.DocName = onerow;
                        DFdata.Add(dfmodel);
                    }
                    catch { };
                }
            }
            return DFdata;
        }
        public List<Model.RIModel> ReadTotalData()
        {
            List<Model.RIModel> Totaldata = new List<Model.RIModel>();
            string[] ripath = Directory.GetFiles(StaticData.RIDataPath);
            foreach (var doc in ripath)
            {
                string readdoc = File.ReadAllText(doc);
                
                foreach (var row in readdoc.Split('\n'))
                {
                    try {
                        Model.RIModel rimodel = new Model.RIModel();
                        List<string> onerow = row.Split(' ').ToList();
                        rimodel.term = onerow[0];
                        rimodel.TF = Convert.ToInt32(onerow[1]);
                        rimodel.DF = Convert.ToInt32(onerow[2]);
                        rimodel.TFDocName = onerow[3];
                        rimodel.WT = Convert.ToDouble(onerow[4]);
                        //rimodel.DFNames = onerow[5].Split(',').ToList();

                        Totaldata.Add(rimodel);
                    }
                    catch { };
                }
            }
            return Totaldata;
        }

        public List<string> ReadStopWordData()
        {
            List<string> StopWordData = new List<string>();
            string[] swpath = Directory.GetFiles(StaticData.StopWordPath);
            foreach (var doc in swpath)
            {
                string readdoc = File.ReadAllText(doc);
                StopWordData = readdoc.Split('\n').ToList();
            }
            StopWordData.RemoveAt(StopWordData.Count() - 1);
            return StopWordData;
        }
        public List<Model.judgementsModel> ReadjudgementsData()
        {
            List<Model.judgementsModel> judgementsData = new List<Model.judgementsModel>();
            string[] rapath = Directory.GetFiles(StaticData.RelativeAssesemntPath);
            foreach (var doc in rapath)
            {
                string readdoc = File.ReadAllText(doc);

                foreach (var line in readdoc.Split('\n'))
                {
                    try {
                        int index = Convert.ToInt16(line.Split(' ')[0]);
                        string relevantdoc = line.Split(' ')[1];
                        if (judgementsData.Where(i => i.Index == index).Count() == 0)
                        {
                            Model.judgementsModel model = new Model.judgementsModel();
                            model.Index = index;
                            model.RelevantDocs.Add(relevantdoc);
                            judgementsData.Add(model);
                        }
                        else
                        {
                            foreach (var item in judgementsData.Where(i => i.Index == index))
                            {
                                item.RelevantDocs.Add(relevantdoc);
                            }
                        }
                    }
                    catch { };
                }
            }
            return judgementsData;
        }

        public List<Model.QuerysModel> ReadQueriesData()
        {
            List<Model.QuerysModel> QueriesData = new List<Model.QuerysModel>();
            string[] Qpath = Directory.GetFiles(StaticData.QueriesPath);
            foreach (var doc in Qpath)
            {
                Model.QuerysModel model = new Model.QuerysModel();
                model.Query = File.ReadAllText(doc);
                model.Index =Convert.ToInt16(doc.Split('\\')[doc.Split('\\').Count() - 1].Split('.')[0]);
                QueriesData.Add(model);
            }
            return QueriesData;
        }
    }
}
