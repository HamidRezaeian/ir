﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.CreateInformation
{
    public class CreateTF
    {
        public bool CreateTFdata(List<Model.DocumentModel> documents)
        {
            try
            {

                foreach (var doc in documents)
                {
                    Write2file(Create(doc.DocumentText), doc.DocumentName);
                }
                CreateDF.CreateDFdata();
                return true;
            }
            catch
            {
                return false;
            }
        }
        private List<Model.TFModel> Create(string DocumentText)
        {
            try
            {
                List<Model.TFModel> TFmodel = new List<Model.TFModel>();
                string[] words = DocumentText.Split('\n');
                List<string> terms = new List<string>();
                //foreach (var word in words)
                //{
                //    TFModel tfmodel = new TFModel();
                //    tfmodel.term = word;
                //    TFmodel.Add(tfmodel);
                //}
                foreach (var word in words)
                {
                    bool isthere = false;
                    foreach (var wordmodel in TFmodel)
                    {
                        
                        if (word==wordmodel.term)
                        {
                            wordmodel.tf++;
                            isthere = true;
                            break;
                        }
                    }

                    if (!isthere)
                    {
                        Model.TFModel tempmodel = new Model.TFModel();
                        tempmodel.term = word;
                        tempmodel.tf = 1;
                        TFmodel.Add(tempmodel);
                    }
                }

                return TFmodel;
            }
            catch
            {
                return null;
            }
        }
        private bool Write2file(List<Model.TFModel> Document,string DocName)
        {
            //path = AppDomain.CurrentDomain.BaseDirectory + path;
            string path = StaticData.TFDataPath;
            try
            {
                Model.UniqueDocListModel uniquemodel = new IRProject.Model.Model.UniqueDocListModel();
                uniquemodel.DocName = DocName;
                uniquemodel.UniqueDocLists = Document;
                CreateDF.UniqueDocLists.Add(uniquemodel);
                File.WriteAllText(path + DocName, Converter.ConvertTFData2String(Document));
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
