﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.CreateInformation
{
    public class CreateTotalInformation
    {
        ReadFile read = new ReadFile();
        public bool CreateTotalinfo()
        {
            try
            {
                StaticData.TFData = read.ReadTFData();
                StaticData.DFData = read.ReadDFData();
                
                StaticData.TotalInformationData.Clear();

                foreach (var df in StaticData.DFData)
                {
                    foreach (var tf in StaticData.TFData.Where(i => i.term == df.Term))
                    {
                        List<string> stopword= StaticData.StopWordData.Where(i => i == tf.term).ToList();
                        if (stopword.Count == 0)
                        {
                            Model.RIModel totalmodel = new Model.RIModel();
                            totalmodel.term = tf.term;
                            totalmodel.TFDocName = tf.DocName;
                            totalmodel.TF = tf.tf;
                            totalmodel.DF = df.DocName.Count();
                            //totalmodel.DFNames = df.DocName;
                            totalmodel.WT = Formula.TfIdf.CalculateLN(totalmodel.TF);
                            StaticData.TotalInformationData.Add(totalmodel);
                        }
                    }

                }
                Write2file(StaticData.TotalInformationData);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private static bool Write2file(List<Model.RIModel> Document)
        {
            try
            {
                File.WriteAllText(StaticData.RIDataPath + StaticData.RIDataFileName, Converter.ConvertTotalData2String(Document));
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
