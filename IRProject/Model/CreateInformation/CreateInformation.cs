﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IRProject.Model.CreateInformation
{
    public class CreateInformation
    {
        ReadFile read = new ReadFile();
        public void CreateTF()
        {
            
            List<Model.DocumentModel> documents = new List<Model.DocumentModel>();
            documents = read.ReadDocuments();

            CreateTF tf = new IRProject.Model.CreateInformation.CreateTF();
            tf.CreateTFdata((List<Model.DocumentModel>)documents);
        }
        public void CreateTotalInformation()
        {
            CreateTotalInformation createtotal = new IRProject.Model.CreateInformation.CreateTotalInformation();
            createtotal.CreateTotalinfo();
        }
        public void CreateStopWord()
        {
            CreateStopWord createtotal = new IRProject.Model.CreateInformation.CreateStopWord();
            createtotal.Create();
        }
    }
}
