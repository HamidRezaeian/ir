﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.CreateInformation
{
    public static class CreateDF
    {
        public static List<Model.UniqueDocListModel> UniqueDocLists = new List<Model.UniqueDocListModel>();

        public static bool CreateDFdata()
        {
            try
            {
                List<Model.DFModel> dfmodel = new List<IRProject.Model.Model.DFModel>();
                //List<DFModel> tempdfmodel = new List<IRProject.Model.CreateInformation.DFModel>();
                bool isthere = false;
                foreach (var uniquelist in UniqueDocLists)
                {
                    foreach (var unique in uniquelist.UniqueDocLists)
                    {
                        //tempdfmodel.Clear();
                        isthere = false;
                        foreach (var df in dfmodel)
                        {
                            if (unique.term == df.Term)
                            {
                                df.DocName.Add(uniquelist.DocName);
                                isthere = true;
                            }
                            //else
                            //{
                            //    DFModel tempmodel = new DFModel();
                            //    tempmodel.Term = unique.term;
                            //    tempmodel.DocName.Add(uniquelist.DocName);
                            //    tempdfmodel.Add(tempmodel);
                            //}
                        }
                        if(!isthere)
                        {
                            Model.DFModel tempmodel = new Model.DFModel();
                            tempmodel.Term = unique.term;
                            tempmodel.DocName.Add(uniquelist.DocName);
                            dfmodel.Add(tempmodel);
                        }
                        //foreach (var temp in tempdfmodel)//CopyTemp2dfModel
                        //{
                        //    dfmodel.Add(temp);
                        //}
                    }

                }
                Write2file(dfmodel);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private static bool Write2file(List<Model.DFModel> Document)
        {
            try
            {
                File.WriteAllText(StaticData.DFDataPath+StaticData.DFDataFileName, Converter.ConvertDFData2String(Document));
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
