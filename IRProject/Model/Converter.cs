﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model
{
    public static class Converter
    {
        
        public static string ConvertTFData2String(List<Model.TFModel> TFData)
        {
            string temp = "";
            foreach (var item in TFData)
            {
                temp += item.term + "\n"+item.tf+"\n";
            }
            return temp;
        }
        public static string ConvertDFData2String(List<Model.DFModel> DFData)
        {
            string temp = "";
            foreach (var df in DFData)
            {
                temp += df.Term + " ";

                foreach (var docname in df.DocName)
                {
                    temp += docname + " ";
                }
                temp += "\n";
            }
            return temp;
        }
        public static string ConvertTotalData2String(List<Model.RIModel> TotalData)
        {
            string temp = "";
            foreach (var item in TotalData)
            {
                temp += item.term + " " + item.TF + " " + item.DF + " " + item.TFDocName + " " + item.WT;
                //foreach (var docname in item.DFNames)
                //{
                //    temp += docname + ",";
                //}
                //temp = temp.Remove(temp.Length-1, 1);
                temp += "\n";
            }
            return temp;
        }
        public static string ConvertStopWordData2String(List<string> StopWordData)
        {
            string temp = "";
            foreach (var item in StopWordData)
            {
                temp += item + "\n";
            }
            temp.Remove(temp.Count() - 1, 1);
            return temp;
        }
    }
}
