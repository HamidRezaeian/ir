﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.ReadInformation
{
    public class ReadInformation
    {
        IRProject.Model.CreateInformation.ReadFile readfile = new CreateInformation.ReadFile();
        public bool Read()
        {
            try
            {
                CreateInformation.CreateInformation create = new CreateInformation.CreateInformation();
                if (!FileIsThere(StaticData.TFDataPath))
                    create.CreateTF();
                else StaticData.N = Directory.GetFiles(StaticData.TFDataPath).Count();

                if (!FileIsThere(StaticData.StopWordPath))
                    create.CreateStopWord();
                else StaticData.StopWordData = readfile.ReadStopWordData();

                if (!FileIsThere(StaticData.RIDataPath))
                    create.CreateTotalInformation();
                else
                {
                    StaticData.TotalInformationData = readfile.ReadTotalData();
                    StaticData.TFData = readfile.ReadTFData();
                    StaticData.DFData = readfile.ReadDFData();
                    StaticData.RelevantData = readfile.ReadjudgementsData();
                    StaticData.QueriesData = readfile.ReadQueriesData();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool FileIsThere(string path)
        {
            string[] files = Directory.GetFiles(path);
            if (files.Count() > 0)
                return true;
            else return false;
        }
    }
}
