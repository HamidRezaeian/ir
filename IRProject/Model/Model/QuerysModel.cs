﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.Model
{
    public class QuerysModel
    {
        public int Index { get; set; }
        public string Query { get; set; }
    }
}
