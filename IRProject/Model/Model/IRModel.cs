﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.Model
{
    public class IRModel
    {
        public string DocName { get; set; }
        public double Score { get; set; }
    }
}
