﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.Model
{
    public class DocumentModel
    {
        public string DocumentText { get; set; }
        public string DocumentName { get; set; }

    }
}
