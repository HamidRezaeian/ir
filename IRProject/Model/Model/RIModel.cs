﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.Model
{
    public class RIModel
    {
        public string term { get; set; }
        public int TF { get; set; }
        public int DF { get; set; }
        public string TFDocName { get; set; }
        public double WT { get; set; } //tf*idf
    }
}
