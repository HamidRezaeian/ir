﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.Model
{
    public class TFModel
    {
        public string term { get; set; }
        public int tf { get; set; }
        public string DocName { get; set; }
    }
}
