﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.Model
{
    public class EvaluationModel
    {
        public string query { get; set; }
        public List<double> Recall = new List<double>();
        public List<double> Precision = new List<double>();
        public double MAP = 0;
        public double FMeasure = 0;
    }
}
