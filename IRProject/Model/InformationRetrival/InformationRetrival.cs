﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model.InformationRetrival
{
    public class InformationRetrival
    {
        public List<Model.IRModel> Search(string query)
        {
            List<Model.TFModel> tfmodel = CalculateTF(query);
            StaticData.Query = tfmodel;
            StaticData.InformationRetreival= CalculateScore(tfmodel).OrderByDescending(i => i.Score).ToList();
            return StaticData.InformationRetreival;
        }
        private List<Model.IRModel> CalculateScore(List<Model.TFModel> tfdata)
        {
            List<Model.IRModel> irmodel = new List<Model.IRModel>();

            List<Model.RIModel> querymodel = CreateQueryModel(tfdata);
            List<Model.RIModel> docmodel = CreateDocModel(tfdata);
            List<double> querywt = new List<double>();
            List<double> _querywt = new List<double>();
            List<double> Docwt = new List<double>();

            querywt = Formula.TfIdf.CalculateCosine(querymodel.Select(i => i.WT).ToList());
            foreach (var docname in docmodel.Select(i => i.TFDocName).Distinct())
            {
                _querywt =new List<double>(querywt);

                Model.IRModel model = new Model.IRModel();

                Docwt = Formula.TfIdf.CalculateCosine(docmodel.Where(i => i.TFDocName == docname).Select(i => i.WT).ToList());
                model.DocName = docname;
                int count_querywt =0;
                for (int i = 0; i < querymodel.Count(); i++)
                {
                    if (docmodel.Where(i1 => i1.term == querymodel[i].term && i1.TFDocName == docname).Count() == 0)
                    {
                        _querywt.RemoveAt(i-count_querywt);
                        count_querywt++;
                    }
                }
                model.Score = Formula.TfIdf.CalculateProd(_querywt, Docwt);
                irmodel.Add(model);
            }

            return irmodel;
        }
        private List<Model.RIModel> CreateQueryModel(List<Model.TFModel> tf)
        {
            List<Model.RIModel> querymodel = new List<Model.RIModel>();
            foreach (var t in tf)
            {
                foreach (var df in StaticData.DFData.Where(i => i.Term == t.term))
                {
                    Model.RIModel rimodel = new Model.RIModel();
                    rimodel.TFDocName = t.DocName;
                    rimodel.term = t.term;
                    rimodel.WT = Formula.TfIdf.CalculateLT(t.tf, df.DocName.Count());
                    querymodel.Add(rimodel);
                }
            }
            return querymodel;
        }
        private List<Model.RIModel> CreateDocModel(List<Model.TFModel> tf)
        {
            List<Model.RIModel> DocModels = new List<Model.RIModel>();
            foreach (var t in tf)
            {
                foreach (var df in StaticData.TotalInformationData.Where(i => i.term == t.term))
                {
                    Model.RIModel rimodel = new Model.RIModel();
                    rimodel.TFDocName = df.TFDocName;
                    rimodel.term = df.term;
                    rimodel.WT = df.WT;
                    DocModels.Add(rimodel);
                }
            }
            return DocModels;
        }
        private List<Model.TFModel> CalculateTF(string query)
        {
            PreProcess.PreProcess preprocess = new PreProcess.PreProcess();
            List<Model.TFModel> tfmodel = new List<Model.TFModel>();
            foreach (var word in preprocess.RUN(query).Split('\n'))
            {
                bool isthere = false;
                foreach (var model in tfmodel)
                {
                    if (model.term == word)
                    {
                        model.tf++;
                        isthere = true;
                        break;
                    }
                }
                if (!isthere)
                {
                    Model.TFModel tempmodel = new Model.TFModel();
                    tempmodel.term = word;
                    tempmodel.tf = 1;
                    if (word!="")
                    tfmodel.Add(tempmodel);
                }
            }
            return tfmodel;
        }
    }
}
