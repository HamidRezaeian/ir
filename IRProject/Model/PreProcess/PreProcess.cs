﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHazm;

namespace IRProject.Model.PreProcess
{
    public class PreProcess
    {
        public string RUN(string DocumentText)
        {
            string output = "";
            //Step1
            Normalizer norm = new Normalizer();
            string Sentences = norm.Run(DocumentText);
            //Step2
            List<string> tokenslist = new List<string>();
            WordTokenizer tokens = new WordTokenizer();
            tokenslist = tokens.Tokenize(Sentences);

            //Step3
            List<string> LemmatizeList = new List<string>();
            Lemmatizer lem = new Lemmatizer();

            for (int i = 0; i < tokenslist.Count; i++)
            {
                try
                {
                    LemmatizeList.Add(lem.Lemmatize(tokenslist[i]));
                }
                catch
                { };
            }

            for (int i = 0; i < LemmatizeList.Count; i++)
            {
                output += LemmatizeList[i] + "\n";
            }

            return output;

        }
    }
}
