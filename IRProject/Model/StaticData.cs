﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRProject.Model
{
    public static class StaticData
    {
        public static string HamshahriCorpusPath = AppDomain.CurrentDomain.BaseDirectory + @"Data\HamshahriData\HamshahriData\HamshahriCorpus\";
        public static string RelativeAssesemntPath = AppDomain.CurrentDomain.BaseDirectory + @"Data\HamshahriData\HamshahriData\RelativeAssesemnt\";
        public static string QueriesPath = AppDomain.CurrentDomain.BaseDirectory + @"Data\HamshahriData\HamshahriData\Queris\";
        public static string StopWordPath = AppDomain.CurrentDomain.BaseDirectory + @"Data\StopWord\";
        public static string RIDataPath = AppDomain.CurrentDomain.BaseDirectory + @"Data\Totalnformation\";
        public static string TFDataPath= AppDomain.CurrentDomain.BaseDirectory + @"Data\TFInfo\";
        public static string DFDataPath = AppDomain.CurrentDomain.BaseDirectory + @"Data\DFInfo\";
        public static string DFDataFileName =  @"DocFreq.DF";
        public static string RIDataFileName = @"TotalInformation.TI";
        public static string StopWordFileName = @"StopWord.SW";

        public static List<Model.EvaluationModel> EvaluationData = new List<Model.EvaluationModel>();
        public static List<Model.QuerysModel> QueriesData = new List<Model.QuerysModel>();
        public static List<Model.judgementsModel> RelevantData = new List<Model.judgementsModel>(); 
        public static List<Model.DFModel> DFData = new List<Model.DFModel>();
        public static List<Model.TFModel> TFData = new List<Model.TFModel>();
        public static List<Model.TFModel> Query = new List<Model.TFModel>();
        
        //public static List<Model.RIModel> Query = new List<Model.RIModel>();
        public static List<Model.RIModel> TotalInformationData = new List<Model.RIModel>();

        public static List<string> StopWordData = new List<string>();
        public static Int32 TotalTerm { get; set; } //  stopword=tf/TotalTerm
        public static Int32 N { get; set; }  //Total Docs

        public static int MaxResult = 20;

        public static List<Model.IRModel> InformationRetreival = new List<Model.IRModel>();

    }
}
