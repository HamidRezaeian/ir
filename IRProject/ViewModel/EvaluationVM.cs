﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
namespace IRProject.ViewModel
{
    public static class EvaluationVM
    {
        public static UserControl Evaluation { get; set; }

        public static void SetData(Model.Model.EvaluationModel model)
        {

            ((View.Main.Main)(ViewModel.MainVM.Main)).Evaluation.Width = 300;
            SetChart(model);
            ((View.Sub.Evaluation.Evaluation)Evaluation).labelMAP.Content =string.Format("MAP = {0}", Math.Round(model.MAP,4));
            ((View.Sub.Evaluation.Evaluation)Evaluation).labelFmeasure.Content = string.Format("FMeasure = {0}", Math.Round(model.FMeasure, 4));

        }
        public static ChartValues<ObservablePoint> Points=new ChartValues<ObservablePoint>();
        private static void SetChart(Model.Model.EvaluationModel model)
        {
            Points.Clear();
            for (int i = 0; i < model.Recall.Count; i++)
            {
                Points.Add(new ObservablePoint(model.Recall[i],model.Precision[i]));
                
            }
        }

    }
}
