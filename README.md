# IR Project

![logo](markdown/ir.png)

This is my project on advanced data retrieval course at pasargad university. I did it individually in three weeks with C# programming language and NHazm library. Also this project was selected as the best project by the course instructor.

## Document collection
In this project, I used a collection of documents from the Hamshahri newspaper from 2003 to 2007. This set of documents consists of three sections of documents, queries and documents related to each question.

## Pre-processing
Document pre-processing includes:
- Normalization
- Tokenization
- lemmatization
- Stopword elimination

Note: for Stop word detection i used this formula: ![swd](markdown/swd.png)  Where X > 0.045

## Indexing
I built the index file with following format:
- term
- document name
- tf (term frequency)
- df (document frequency)
- wt (tf*idf)

## Search and retrieve documents
Sequential search in tf-idf vector space: i used lnc.ltc as a standard weighting scheme.

## System evaluation
![evaluation](markdown/eval.png)

Due to the having search results and the identification of relevant and unrelated documents i used two methods of FMeasure and MAP for evaluation.
To calculate Fmeasure, we obtain precision and recall, and we calculate for each query entered, Also with each new queue, the MAP value will be updated.
#### It should be noted that recall is calculated based on 20 retrieved documents.

## Requirement:
- [x] .NET Framework
